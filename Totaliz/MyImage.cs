﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Totaliz
{
    public class MyImage
    {
        private string bigImg, smallImg;

        [XmlElement("big")]
        public string BigImg
        {
            get { return bigImg; }
            set
            {
                if (File.Exists(".//img//" + value))
                    try
                    {
                        this.BigImage = (Image)new Bitmap(".//img//" + value);
                        bigImg = value;
                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.Debug.WriteLine("Error! " + e.ToString() + " \n" + e.Message);
                    }
                else
                    System.Diagnostics.Debug.WriteLine("File " + value + " not found!");
            }
        }

        [XmlElement("small")]
        public string SmallImg
        {
            get { return smallImg; }
            set
            {
                if (File.Exists(".//img//" + value))
                    try
                    {
                        this.SmallImage = (Image)new Bitmap(".//img//" + value);
                        smallImg = value;
                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.Debug.WriteLine("Error! " + e.ToString() + " \n" + e.Message);
                    }
                else
                    System.Diagnostics.Debug.WriteLine("File " + value + " not found!");

            }
        }

        public string Id { get; set; }
        public string Name { get; set; }
        [XmlElement("cat")]
        public string Category { get; set; }

        [XmlIgnore]
        public Image BigImage { get; set; }
        [XmlIgnore]
        public Image SmallImage { get; set; }

        public MyImage()
        {

        }
    }
}
