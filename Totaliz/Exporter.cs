﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Reflection;
using Microsoft.Office;
using Microsoft.Office.Core;
using Excel = Microsoft.Office.Interop.Excel;

namespace Totaliz
{
    class Exporter
    {
        public static void ExportTeams(BindingList<Team> teams)
        {
            if (teams.Count == 0)
            {
                System.Windows.Forms.MessageBox.Show("Список команд пустий. Нічого експортувати.", "Список команд пустий", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
                return;
            }

            Excel.Application app = new Excel.Application();

            Excel.Workbook workbook = app.Workbooks.Add(Missing.Value);
            app.Visible = true;

            Excel.Worksheet worksheet = workbook.Worksheets[1];

            Excel.Range range = worksheet.get_Range("C2");
            range.Value2 = "Назва";
            range.ColumnWidth = 25;

            range = worksheet.get_Range("D2");
            range.Value2 = "Рейтинг ELO";
            range.ColumnWidth = 15;

            range = worksheet.get_Range("E2");
            range.Value2 = "Сума очок";
            range.ColumnWidth = 15;

            range = worksheet.get_Range("F2");
            range.Value2 = "Перемоги";
            range.ColumnWidth = 10;

            range = worksheet.get_Range("G2");
            range.Value2 = "Нічиї";
            range.ColumnWidth = 10;

            range = worksheet.get_Range("H2");
            range.Value2 = "Поразки";
            range.ColumnWidth = 10;

            range = worksheet.get_Range("C2", "H2");
            range.HorizontalAlignment = Excel.Constants.xlCenter;
            range.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
            range.Borders.Weight=Excel.XlBorderWeight.xlThick;

            int i;
            for (i = 0; i < teams.Count; i++)
            {
                Team t = teams[i];

                worksheet.get_Range("B" + (3 + i)).Value2 = (i + 1).ToString() + ". ";
                worksheet.get_Range("C" + (3 + i)).Value2 = t.Name;
                worksheet.get_Range("D" + (3 + i)).Value2 = t.RatingELO;
                worksheet.get_Range("E" + (3 + i)).Value2 = t.RatingNNetwork;
                worksheet.get_Range("F" + (3 + i)).Value2 = t.Win;
                worksheet.get_Range("G" + (3 + i)).Value2 = t.Draw;
                worksheet.get_Range("H" + (3 + i)).Value2 = t.Defeat;
            }

            range = worksheet.get_Range("B2");
            range.Borders[Excel.XlBordersIndex.xlDiagonalUp].LineStyle = Excel.XlLineStyle.xlContinuous;
            range.Borders[Excel.XlBordersIndex.xlDiagonalUp].Weight = Excel.XlBorderWeight.xlThick;

            range = worksheet.get_Range("B3", "B" + (i + 2));
            range.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
            range.Borders.Weight = Excel.XlBorderWeight.xlThick;

            range = worksheet.get_Range("C3", "H" + (i + 2));
            range.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
            range.Borders.Weight = Excel.XlBorderWeight.xlThin;
        }

        public static void ExportGames(BindingList<Game> games)
        {
            if (games.Count == 0)
            {
                System.Windows.Forms.MessageBox.Show("Список ігор пустий. Нічого експортувати.", "Список ігор пустий", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
                return;
            }

            Excel.Application app = new Excel.Application();

            Excel.Workbook workbook = app.Workbooks.Add(Missing.Value);
            app.Visible = true;

            Excel.Worksheet worksheet = workbook.Worksheets[1];

            Excel.Range range = worksheet.get_Range("C2");
            range.Value2 = "Команда А";
            range.ColumnWidth = 25;

            range = worksheet.get_Range("D2");
            range.Value2 = "Команда B";
            range.ColumnWidth = 25;

            range = worksheet.get_Range("E2");
            range.Value2 = "Дата";
            range.ColumnWidth = 15;

            range = worksheet.get_Range("F2");
            range.Value2 = "Рахунок";
            range.ColumnWidth = 15;

            range = worksheet.get_Range("G2");
            range.Value2 = "Прогноз";
            range.ColumnWidth = 15;

            range = worksheet.get_Range("C2", "G2");
            range.HorizontalAlignment = Excel.Constants.xlCenter;
            range.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
            range.Borders.Weight = Excel.XlBorderWeight.xlThick;

            int i;
            for (i = 0; i < games.Count; i++)
            {
                Game g = games[i];

                worksheet.get_Range("B" + (3 + i)).Value2 = (i + 1).ToString() + ". ";
                worksheet.get_Range("C" + (3 + i)).Value2 = g.A;
                worksheet.get_Range("D" + (3 + i)).Value2 = g.B;
                worksheet.get_Range("E" + (3 + i)).Value2 = g.Date.ToShortDateString();

                range = worksheet.get_Range("F" + (3 + i));
                range.Clear();
                range.NumberFormat = "@";
                range.Value2 = g.MatchEnd == null ? "" : g.MatchEnd.Replace(":", " : ");

                worksheet.get_Range("G" + (3 + i)).Value2 = g.Prediction;

                double a, b;

                if (g.Complete)
                {
                    a = Convert.ToDouble(g.MatchEnd.Split(':')[0]);
                    b = Convert.ToDouble(g.MatchEnd.Split(':')[1]);
                }
                else 
                {
                    a = Convert.ToDouble(g.Prediction.Split(':')[0]);
                    b = Convert.ToDouble(g.Prediction.Split(':')[1]);
                }

                if (a > b)
                {
                    worksheet.get_Range("C" + (3 + i)).Font.Bold = true;
                }
                else if (b > a)
                {
                    worksheet.get_Range("D" + (3 + i)).Font.Bold = true;
                }
                else 
                {
                    worksheet.get_Range("C" + (3 + i)).Font.Italic = true;
                    worksheet.get_Range("D" + (3 + i)).Font.Italic = true;
                }
            }

            range = worksheet.get_Range("B2");
            range.Borders[Excel.XlBordersIndex.xlDiagonalUp].LineStyle = Excel.XlLineStyle.xlContinuous;
            range.Borders[Excel.XlBordersIndex.xlDiagonalUp].Weight = Excel.XlBorderWeight.xlThick;

            range = worksheet.get_Range("B3", "B" + (i + 2));
            range.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
            range.Borders.Weight = Excel.XlBorderWeight.xlThick;

            range = worksheet.get_Range("C3", "G" + (i + 2));
            range.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
            range.Borders.Weight = Excel.XlBorderWeight.xlThin;
        }
    }
}
