﻿namespace Totaliz
{
    partial class ChampionshipForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChampionshipForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.teamTabPage = new System.Windows.Forms.TabPage();
            this.updateRatingButton = new System.Windows.Forms.Button();
            this.delTeamButton = new System.Windows.Forms.Button();
            this.teamsDataGridView = new System.Windows.Forms.DataGridView();
            this.ImgColumn = new System.Windows.Forms.DataGridViewImageColumn();
            this.NameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RatingELOColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RatingNNetworkColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WinColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DefeatColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DrawColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emptyTeamsColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teamsContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.змінитиКToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.видалитиКToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editTeamButton = new System.Windows.Forms.Button();
            this.addTeamButton = new System.Windows.Forms.Button();
            this.gamesTabPage = new System.Windows.Forms.TabPage();
            this.updatePredictionButton = new System.Windows.Forms.Button();
            this.delGameButton = new System.Windows.Forms.Button();
            this.editGameButton = new System.Windows.Forms.Button();
            this.addGamebutton = new System.Windows.Forms.Button();
            this.gamesDataGridView = new System.Windows.Forms.DataGridView();
            this.ATeamColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BTeamColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MatchColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PredictionColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emptyGemesColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gamesContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.змінитиІToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.видалитиІToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.зберегтиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.зберегтиЯкToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.закритиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.експортToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ігриToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.командиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.командаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addTeamToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editTeamToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteTeamToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.граToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addGameToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.editGameToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteGameToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.рейтингToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.оновитиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.показатиІсторіюToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl.SuspendLayout();
            this.teamTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teamsDataGridView)).BeginInit();
            this.teamsContextMenuStrip.SuspendLayout();
            this.gamesTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gamesDataGridView)).BeginInit();
            this.gamesContextMenuStrip.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.teamTabPage);
            this.tabControl.Controls.Add(this.gamesTabPage);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(581, 298);
            this.tabControl.TabIndex = 0;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // teamTabPage
            // 
            this.teamTabPage.Controls.Add(this.updateRatingButton);
            this.teamTabPage.Controls.Add(this.delTeamButton);
            this.teamTabPage.Controls.Add(this.teamsDataGridView);
            this.teamTabPage.Controls.Add(this.editTeamButton);
            this.teamTabPage.Controls.Add(this.addTeamButton);
            this.teamTabPage.Cursor = System.Windows.Forms.Cursors.Default;
            this.teamTabPage.Location = new System.Drawing.Point(4, 22);
            this.teamTabPage.Name = "teamTabPage";
            this.teamTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.teamTabPage.Size = new System.Drawing.Size(573, 272);
            this.teamTabPage.TabIndex = 0;
            this.teamTabPage.Text = "Команди";
            this.teamTabPage.UseVisualStyleBackColor = true;
            // 
            // updateRatingButton
            // 
            this.updateRatingButton.Image = global::Totaliz.Properties.Resources.update17;
            this.updateRatingButton.Location = new System.Drawing.Point(285, 6);
            this.updateRatingButton.Name = "updateRatingButton";
            this.updateRatingButton.Size = new System.Drawing.Size(130, 33);
            this.updateRatingButton.TabIndex = 5;
            this.updateRatingButton.Text = " Оновити рейтинг...";
            this.updateRatingButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.updateRatingButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.updateRatingButton.UseVisualStyleBackColor = true;
            this.updateRatingButton.Click += new System.EventHandler(this.updateButton_Click);
            // 
            // delTeamButton
            // 
            this.delTeamButton.Image = global::Totaliz.Properties.Resources.delete85;
            this.delTeamButton.Location = new System.Drawing.Point(188, 6);
            this.delTeamButton.Name = "delTeamButton";
            this.delTeamButton.Size = new System.Drawing.Size(91, 33);
            this.delTeamButton.TabIndex = 4;
            this.delTeamButton.Text = " Видалити...";
            this.delTeamButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.delTeamButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.delTeamButton.UseVisualStyleBackColor = true;
            this.delTeamButton.Click += new System.EventHandler(this.delTeamButton_Click);
            // 
            // teamsDataGridView
            // 
            this.teamsDataGridView.AllowUserToAddRows = false;
            this.teamsDataGridView.AllowUserToDeleteRows = false;
            this.teamsDataGridView.AllowUserToResizeRows = false;
            this.teamsDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.teamsDataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.teamsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.teamsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ImgColumn,
            this.NameColumn,
            this.RatingELOColumn,
            this.RatingNNetworkColumn,
            this.WinColumn,
            this.DefeatColumn,
            this.DrawColumn,
            this.emptyTeamsColumn});
            this.teamsDataGridView.ContextMenuStrip = this.teamsContextMenuStrip;
            this.teamsDataGridView.Cursor = System.Windows.Forms.Cursors.Default;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.MenuHighlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.teamsDataGridView.DefaultCellStyle = dataGridViewCellStyle5;
            this.teamsDataGridView.GridColor = System.Drawing.SystemColors.Control;
            this.teamsDataGridView.Location = new System.Drawing.Point(8, 45);
            this.teamsDataGridView.MultiSelect = false;
            this.teamsDataGridView.Name = "teamsDataGridView";
            this.teamsDataGridView.ReadOnly = true;
            this.teamsDataGridView.RowHeadersVisible = false;
            this.teamsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.teamsDataGridView.Size = new System.Drawing.Size(559, 224);
            this.teamsDataGridView.TabIndex = 3;
            this.teamsDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.teamsDataGridView_CellClick);
            this.teamsDataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.teamsDataGridView_CellDoubleClick);
            // 
            // ImgColumn
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.NullValue = ((object)(resources.GetObject("dataGridViewCellStyle4.NullValue")));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.ImgColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.ImgColumn.FillWeight = 10F;
            this.ImgColumn.HeaderText = "Логотип";
            this.ImgColumn.Name = "ImgColumn";
            this.ImgColumn.ReadOnly = true;
            // 
            // NameColumn
            // 
            this.NameColumn.FillWeight = 30F;
            this.NameColumn.HeaderText = "Назва";
            this.NameColumn.Name = "NameColumn";
            this.NameColumn.ReadOnly = true;
            this.NameColumn.Width = 250;
            // 
            // RatingELOColumn
            // 
            this.RatingELOColumn.FillWeight = 10F;
            this.RatingELOColumn.HeaderText = "Рейтинг 1";
            this.RatingELOColumn.Name = "RatingELOColumn";
            this.RatingELOColumn.ReadOnly = true;
            this.RatingELOColumn.Width = 82;
            // 
            // RatingNNetworkColumn
            // 
            this.RatingNNetworkColumn.FillWeight = 10F;
            this.RatingNNetworkColumn.HeaderText = "Рейтинг 2";
            this.RatingNNetworkColumn.Name = "RatingNNetworkColumn";
            this.RatingNNetworkColumn.ReadOnly = true;
            this.RatingNNetworkColumn.Width = 82;
            // 
            // WinColumn
            // 
            this.WinColumn.FillWeight = 10F;
            this.WinColumn.HeaderText = "Перемоги";
            this.WinColumn.Name = "WinColumn";
            this.WinColumn.ReadOnly = true;
            // 
            // DefeatColumn
            // 
            this.DefeatColumn.FillWeight = 10F;
            this.DefeatColumn.HeaderText = "Поразки";
            this.DefeatColumn.Name = "DefeatColumn";
            this.DefeatColumn.ReadOnly = true;
            // 
            // DrawColumn
            // 
            this.DrawColumn.FillWeight = 10F;
            this.DrawColumn.HeaderText = "Нічиї";
            this.DrawColumn.Name = "DrawColumn";
            this.DrawColumn.ReadOnly = true;
            // 
            // emptyTeamsColumn
            // 
            this.emptyTeamsColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.emptyTeamsColumn.FillWeight = 1F;
            this.emptyTeamsColumn.HeaderText = "";
            this.emptyTeamsColumn.MinimumWidth = 2;
            this.emptyTeamsColumn.Name = "emptyTeamsColumn";
            this.emptyTeamsColumn.ReadOnly = true;
            // 
            // teamsContextMenuStrip
            // 
            this.teamsContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.змінитиКToolStripMenuItem,
            this.видалитиКToolStripMenuItem});
            this.teamsContextMenuStrip.Name = "contextMenuStrip1";
            this.teamsContextMenuStrip.ShowImageMargin = false;
            this.teamsContextMenuStrip.Size = new System.Drawing.Size(111, 48);
            this.teamsContextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.teamsContextMenuStrip_Opening);
            // 
            // змінитиКToolStripMenuItem
            // 
            this.змінитиКToolStripMenuItem.Name = "змінитиКToolStripMenuItem";
            this.змінитиКToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.змінитиКToolStripMenuItem.Text = "Змінити...";
            this.змінитиКToolStripMenuItem.Click += new System.EventHandler(this.editTeamButton_Click);
            // 
            // видалитиКToolStripMenuItem
            // 
            this.видалитиКToolStripMenuItem.Name = "видалитиКToolStripMenuItem";
            this.видалитиКToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.видалитиКToolStripMenuItem.Text = "Видалити...";
            this.видалитиКToolStripMenuItem.Click += new System.EventHandler(this.delTeamButton_Click);
            // 
            // editTeamButton
            // 
            this.editTeamButton.Image = global::Totaliz.Properties.Resources.pencil125;
            this.editTeamButton.Location = new System.Drawing.Point(96, 6);
            this.editTeamButton.Name = "editTeamButton";
            this.editTeamButton.Size = new System.Drawing.Size(86, 33);
            this.editTeamButton.TabIndex = 2;
            this.editTeamButton.Text = " Змінити...";
            this.editTeamButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.editTeamButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.editTeamButton.UseVisualStyleBackColor = true;
            this.editTeamButton.Click += new System.EventHandler(this.editTeamButton_Click);
            // 
            // addTeamButton
            // 
            this.addTeamButton.Image = global::Totaliz.Properties.Resources.hospital25;
            this.addTeamButton.Location = new System.Drawing.Point(8, 6);
            this.addTeamButton.Name = "addTeamButton";
            this.addTeamButton.Size = new System.Drawing.Size(82, 33);
            this.addTeamButton.TabIndex = 1;
            this.addTeamButton.Text = " Додати...";
            this.addTeamButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.addTeamButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.addTeamButton.UseVisualStyleBackColor = true;
            this.addTeamButton.Click += new System.EventHandler(this.addTeamButton_Click);
            // 
            // gamesTabPage
            // 
            this.gamesTabPage.Controls.Add(this.updatePredictionButton);
            this.gamesTabPage.Controls.Add(this.delGameButton);
            this.gamesTabPage.Controls.Add(this.editGameButton);
            this.gamesTabPage.Controls.Add(this.addGamebutton);
            this.gamesTabPage.Controls.Add(this.gamesDataGridView);
            this.gamesTabPage.Location = new System.Drawing.Point(4, 22);
            this.gamesTabPage.Name = "gamesTabPage";
            this.gamesTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.gamesTabPage.Size = new System.Drawing.Size(573, 272);
            this.gamesTabPage.TabIndex = 1;
            this.gamesTabPage.Text = "Ігри";
            this.gamesTabPage.UseVisualStyleBackColor = true;
            // 
            // updatePredictionButton
            // 
            this.updatePredictionButton.Image = global::Totaliz.Properties.Resources.update17;
            this.updatePredictionButton.Location = new System.Drawing.Point(285, 6);
            this.updatePredictionButton.Name = "updatePredictionButton";
            this.updatePredictionButton.Size = new System.Drawing.Size(130, 33);
            this.updatePredictionButton.TabIndex = 10;
            this.updatePredictionButton.Text = " Оновити прогноз...";
            this.updatePredictionButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.updatePredictionButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.updatePredictionButton.UseVisualStyleBackColor = true;
            this.updatePredictionButton.Click += new System.EventHandler(this.updateButton_Click);
            // 
            // delGameButton
            // 
            this.delGameButton.Image = global::Totaliz.Properties.Resources.delete85;
            this.delGameButton.Location = new System.Drawing.Point(188, 6);
            this.delGameButton.Name = "delGameButton";
            this.delGameButton.Size = new System.Drawing.Size(91, 33);
            this.delGameButton.TabIndex = 9;
            this.delGameButton.Text = " Видалити...";
            this.delGameButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.delGameButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.delGameButton.UseVisualStyleBackColor = true;
            this.delGameButton.Click += new System.EventHandler(this.delGameButton_Click);
            // 
            // editGameButton
            // 
            this.editGameButton.Image = global::Totaliz.Properties.Resources.pencil125;
            this.editGameButton.Location = new System.Drawing.Point(96, 6);
            this.editGameButton.Name = "editGameButton";
            this.editGameButton.Size = new System.Drawing.Size(86, 33);
            this.editGameButton.TabIndex = 8;
            this.editGameButton.Text = " Змінити...";
            this.editGameButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.editGameButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.editGameButton.UseVisualStyleBackColor = true;
            this.editGameButton.Click += new System.EventHandler(this.editGameButton_Click);
            // 
            // addGamebutton
            // 
            this.addGamebutton.Image = global::Totaliz.Properties.Resources.hospital25;
            this.addGamebutton.Location = new System.Drawing.Point(8, 6);
            this.addGamebutton.Name = "addGamebutton";
            this.addGamebutton.Size = new System.Drawing.Size(82, 33);
            this.addGamebutton.TabIndex = 7;
            this.addGamebutton.Text = " Додати...";
            this.addGamebutton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.addGamebutton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.addGamebutton.UseVisualStyleBackColor = true;
            this.addGamebutton.Click += new System.EventHandler(this.addGamebutton_Click);
            // 
            // gamesDataGridView
            // 
            this.gamesDataGridView.AllowUserToAddRows = false;
            this.gamesDataGridView.AllowUserToDeleteRows = false;
            this.gamesDataGridView.AllowUserToResizeRows = false;
            this.gamesDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gamesDataGridView.BackgroundColor = System.Drawing.SystemColors.Control;
            this.gamesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gamesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ATeamColumn,
            this.BTeamColumn,
            this.DateColumn,
            this.MatchColumn,
            this.PredictionColumn,
            this.emptyGemesColumn});
            this.gamesDataGridView.ContextMenuStrip = this.gamesContextMenuStrip;
            this.gamesDataGridView.Cursor = System.Windows.Forms.Cursors.Default;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gamesDataGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.gamesDataGridView.GridColor = System.Drawing.SystemColors.Control;
            this.gamesDataGridView.Location = new System.Drawing.Point(8, 45);
            this.gamesDataGridView.MultiSelect = false;
            this.gamesDataGridView.Name = "gamesDataGridView";
            this.gamesDataGridView.ReadOnly = true;
            this.gamesDataGridView.RowHeadersVisible = false;
            this.gamesDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gamesDataGridView.Size = new System.Drawing.Size(559, 224);
            this.gamesDataGridView.TabIndex = 4;
            this.gamesDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gamesDataGridView_CellClick);
            this.gamesDataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gamesDataGridView_CellDoubleClick);
            // 
            // ATeamColumn
            // 
            this.ATeamColumn.HeaderText = "Команда А";
            this.ATeamColumn.Name = "ATeamColumn";
            this.ATeamColumn.ReadOnly = true;
            this.ATeamColumn.Width = 150;
            // 
            // BTeamColumn
            // 
            this.BTeamColumn.HeaderText = "Команда Б";
            this.BTeamColumn.Name = "BTeamColumn";
            this.BTeamColumn.ReadOnly = true;
            this.BTeamColumn.Width = 150;
            // 
            // DateColumn
            // 
            this.DateColumn.HeaderText = "Дата";
            this.DateColumn.Name = "DateColumn";
            this.DateColumn.ReadOnly = true;
            this.DateColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // MatchColumn
            // 
            this.MatchColumn.HeaderText = "Рахунок";
            this.MatchColumn.Name = "MatchColumn";
            this.MatchColumn.ReadOnly = true;
            // 
            // PredictionColumn
            // 
            this.PredictionColumn.HeaderText = "Прогноз";
            this.PredictionColumn.Name = "PredictionColumn";
            this.PredictionColumn.ReadOnly = true;
            // 
            // emptyGemesColumn
            // 
            this.emptyGemesColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.emptyGemesColumn.FillWeight = 1F;
            this.emptyGemesColumn.HeaderText = "";
            this.emptyGemesColumn.MinimumWidth = 2;
            this.emptyGemesColumn.Name = "emptyGemesColumn";
            this.emptyGemesColumn.ReadOnly = true;
            // 
            // gamesContextMenuStrip
            // 
            this.gamesContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.змінитиІToolStripMenuItem,
            this.видалитиІToolStripMenuItem});
            this.gamesContextMenuStrip.Name = "gamesContextMenuStrip";
            this.gamesContextMenuStrip.ShowImageMargin = false;
            this.gamesContextMenuStrip.Size = new System.Drawing.Size(111, 48);
            this.gamesContextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.gamesContextMenuStrip_Opening);
            // 
            // змінитиІToolStripMenuItem
            // 
            this.змінитиІToolStripMenuItem.Name = "змінитиІToolStripMenuItem";
            this.змінитиІToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.змінитиІToolStripMenuItem.Text = "Змінити...";
            this.змінитиІToolStripMenuItem.Click += new System.EventHandler(this.editGameButton_Click);
            // 
            // видалитиІToolStripMenuItem
            // 
            this.видалитиІToolStripMenuItem.Name = "видалитиІToolStripMenuItem";
            this.видалитиІToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.видалитиІToolStripMenuItem.Text = "Видалити...";
            this.видалитиІToolStripMenuItem.Click += new System.EventHandler(this.delGameButton_Click);
            // 
            // menuStrip
            // 
            this.menuStrip.AllowItemReorder = true;
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.командаToolStripMenuItem,
            this.граToolStripMenuItem,
            this.рейтингToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(581, 24);
            this.menuStrip.TabIndex = 1;
            this.menuStrip.Text = "menuStrip1";
            this.menuStrip.Visible = false;
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.зберегтиToolStripMenuItem,
            this.зберегтиЯкToolStripMenuItem,
            this.закритиToolStripMenuItem,
            this.експортToolStripMenuItem,
            this.toolStripSeparator1});
            this.файлToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.MatchOnly;
            this.файлToolStripMenuItem.MergeIndex = 1;
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // зберегтиToolStripMenuItem
            // 
            this.зберегтиToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
            this.зберегтиToolStripMenuItem.MergeIndex = 3;
            this.зберегтиToolStripMenuItem.Name = "зберегтиToolStripMenuItem";
            this.зберегтиToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.зберегтиToolStripMenuItem.Text = "Зберегти...";
            this.зберегтиToolStripMenuItem.Click += new System.EventHandler(this.зберегтиToolStripMenuItem_Click);
            // 
            // зберегтиЯкToolStripMenuItem
            // 
            this.зберегтиЯкToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
            this.зберегтиЯкToolStripMenuItem.MergeIndex = 4;
            this.зберегтиЯкToolStripMenuItem.Name = "зберегтиЯкToolStripMenuItem";
            this.зберегтиЯкToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.зберегтиЯкToolStripMenuItem.Text = "Зберегти як...";
            this.зберегтиЯкToolStripMenuItem.Click += new System.EventHandler(this.зберегтиЯкToolStripMenuItem_Click);
            // 
            // закритиToolStripMenuItem
            // 
            this.закритиToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
            this.закритиToolStripMenuItem.MergeIndex = 5;
            this.закритиToolStripMenuItem.Name = "закритиToolStripMenuItem";
            this.закритиToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.закритиToolStripMenuItem.Text = "Закрити";
            this.закритиToolStripMenuItem.Click += new System.EventHandler(this.закритиToolStripMenuItem_Click);
            // 
            // експортToolStripMenuItem
            // 
            this.експортToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ігриToolStripMenuItem,
            this.командиToolStripMenuItem});
            this.експортToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
            this.експортToolStripMenuItem.MergeIndex = 6;
            this.експортToolStripMenuItem.Name = "експортToolStripMenuItem";
            this.експортToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.експортToolStripMenuItem.Text = "Експорт в Excel";
            // 
            // ігриToolStripMenuItem
            // 
            this.ігриToolStripMenuItem.Name = "ігриToolStripMenuItem";
            this.ігриToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.ігриToolStripMenuItem.Text = "Ігри";
            this.ігриToolStripMenuItem.Click += new System.EventHandler(this.ігриToolStripMenuItem_Click);
            // 
            // командиToolStripMenuItem
            // 
            this.командиToolStripMenuItem.Name = "командиToolStripMenuItem";
            this.командиToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.командиToolStripMenuItem.Text = "Команди";
            this.командиToolStripMenuItem.Click += new System.EventHandler(this.командиToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.MergeAction = System.Windows.Forms.MergeAction.Insert;
            this.toolStripSeparator1.MergeIndex = 7;
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(153, 6);
            // 
            // командаToolStripMenuItem
            // 
            this.командаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addTeamToolStripMenuItem,
            this.editTeamToolStripMenuItem,
            this.deleteTeamToolStripMenuItem});
            this.командаToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
            this.командаToolStripMenuItem.MergeIndex = 2;
            this.командаToolStripMenuItem.Name = "командаToolStripMenuItem";
            this.командаToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.командаToolStripMenuItem.Text = "Команда";
            // 
            // addTeamToolStripMenuItem
            // 
            this.addTeamToolStripMenuItem.Name = "addTeamToolStripMenuItem";
            this.addTeamToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.addTeamToolStripMenuItem.Text = "Додати...";
            this.addTeamToolStripMenuItem.Click += new System.EventHandler(this.addTeamButton_Click);
            // 
            // editTeamToolStripMenuItem
            // 
            this.editTeamToolStripMenuItem.Name = "editTeamToolStripMenuItem";
            this.editTeamToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.editTeamToolStripMenuItem.Text = "Змінити...";
            this.editTeamToolStripMenuItem.Click += new System.EventHandler(this.editTeamButton_Click);
            // 
            // deleteTeamToolStripMenuItem
            // 
            this.deleteTeamToolStripMenuItem.Name = "deleteTeamToolStripMenuItem";
            this.deleteTeamToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.deleteTeamToolStripMenuItem.Text = "Видалити...";
            this.deleteTeamToolStripMenuItem.Click += new System.EventHandler(this.delTeamButton_Click);
            // 
            // граToolStripMenuItem
            // 
            this.граToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addGameToolStripMenuItem1,
            this.editGameToolStripMenuItem1,
            this.deleteGameToolStripMenuItem1});
            this.граToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
            this.граToolStripMenuItem.MergeIndex = 3;
            this.граToolStripMenuItem.Name = "граToolStripMenuItem";
            this.граToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
            this.граToolStripMenuItem.Text = "Гра";
            // 
            // addGameToolStripMenuItem1
            // 
            this.addGameToolStripMenuItem1.Name = "addGameToolStripMenuItem1";
            this.addGameToolStripMenuItem1.Size = new System.Drawing.Size(135, 22);
            this.addGameToolStripMenuItem1.Text = "Додати...";
            this.addGameToolStripMenuItem1.Click += new System.EventHandler(this.addGamebutton_Click);
            // 
            // editGameToolStripMenuItem1
            // 
            this.editGameToolStripMenuItem1.Name = "editGameToolStripMenuItem1";
            this.editGameToolStripMenuItem1.Size = new System.Drawing.Size(135, 22);
            this.editGameToolStripMenuItem1.Text = "Змінити...";
            this.editGameToolStripMenuItem1.Click += new System.EventHandler(this.editGameButton_Click);
            // 
            // deleteGameToolStripMenuItem1
            // 
            this.deleteGameToolStripMenuItem1.Name = "deleteGameToolStripMenuItem1";
            this.deleteGameToolStripMenuItem1.Size = new System.Drawing.Size(135, 22);
            this.deleteGameToolStripMenuItem1.Text = "Видалити...";
            this.deleteGameToolStripMenuItem1.Click += new System.EventHandler(this.delGameButton_Click);
            // 
            // рейтингToolStripMenuItem
            // 
            this.рейтингToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.оновитиToolStripMenuItem,
            this.показатиІсторіюToolStripMenuItem});
            this.рейтингToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
            this.рейтингToolStripMenuItem.MergeIndex = 4;
            this.рейтингToolStripMenuItem.Name = "рейтингToolStripMenuItem";
            this.рейтингToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.рейтингToolStripMenuItem.Text = "Рейтинг";
            this.рейтингToolStripMenuItem.Visible = false;
            // 
            // оновитиToolStripMenuItem
            // 
            this.оновитиToolStripMenuItem.Name = "оновитиToolStripMenuItem";
            this.оновитиToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.оновитиToolStripMenuItem.Text = "Оновити";
            this.оновитиToolStripMenuItem.Click += new System.EventHandler(this.updateButton_Click);
            // 
            // показатиІсторіюToolStripMenuItem
            // 
            this.показатиІсторіюToolStripMenuItem.Name = "показатиІсторіюToolStripMenuItem";
            this.показатиІсторіюToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.показатиІсторіюToolStripMenuItem.Text = "Показати історію";
            this.показатиІсторіюToolStripMenuItem.Visible = false;
            this.показатиІсторіюToolStripMenuItem.Click += new System.EventHandler(this.показатиІсторіюToolStripMenuItem_Click);
            // 
            // ChampionshipForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(581, 298);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.Name = "ChampionshipForm";
            this.Text = "ChampionshipForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ChampionshipForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ChampionshipForm_FormClosed);
            this.Load += new System.EventHandler(this.ChampionshipForm_Load);
            this.ParentChanged += new System.EventHandler(this.ChampionshipForm_ParentChanged);
            this.tabControl.ResumeLayout(false);
            this.teamTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.teamsDataGridView)).EndInit();
            this.teamsContextMenuStrip.ResumeLayout(false);
            this.gamesTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gamesDataGridView)).EndInit();
            this.gamesContextMenuStrip.ResumeLayout(false);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage teamTabPage;
        private System.Windows.Forms.TabPage gamesTabPage;
        private System.Windows.Forms.Button editTeamButton;
        private System.Windows.Forms.Button addTeamButton;
        private System.Windows.Forms.DataGridView teamsDataGridView;
        private System.Windows.Forms.DataGridView gamesDataGridView;
        private System.Windows.Forms.Button delTeamButton;
        private System.Windows.Forms.Button delGameButton;
        private System.Windows.Forms.Button editGameButton;
        private System.Windows.Forms.Button addGamebutton;
        private System.Windows.Forms.DataGridViewTextBoxColumn ATeamColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn BTeamColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn MatchColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn PredictionColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn emptyGemesColumn;
        private System.Windows.Forms.Button updateRatingButton;
        private System.Windows.Forms.Button updatePredictionButton;
        private System.Windows.Forms.DataGridViewImageColumn ImgColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn RatingELOColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn RatingNNetworkColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn WinColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DefeatColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DrawColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn emptyTeamsColumn;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem зберегтиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem зберегтиЯкToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem закритиToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem експортToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ігриToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem командиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem командаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addTeamToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editTeamToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteTeamToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem граToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addGameToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem editGameToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem deleteGameToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem рейтингToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem оновитиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem показатиІсторіюToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip teamsContextMenuStrip;
        private System.Windows.Forms.ContextMenuStrip gamesContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem змінитиІToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem видалитиІToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem змінитиКToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem видалитиКToolStripMenuItem;
    }
}