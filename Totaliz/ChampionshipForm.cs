﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
using System.IO;

namespace Totaliz
{
    public partial class ChampionshipForm : Form
    {
        string name;
        string path;

        Random r = new Random();

        SaveFileDialog saveFileDialog;

        private BindingList<Team> teams;
        private BindingList<Game> games;

        private SortOrder teamsSortOrder, gamesSortOrder;

        public ToolStripMenuItem MenuItem { get; set; }
        public bool OpenOk { get; set; }

        private ChampionshipForm() 
        {
            InitializeComponent();

            teamsSortOrder = SortOrder.None;
            gamesSortOrder = SortOrder.None;
        }

        public ChampionshipForm(int i) : this()
        {
            teams = new BindingList<Team>();

            Game.Teams = teams;

            BindTeams();

            games = new BindingList<Game>();

            BindGames();

            name = "Без назви " + i;
            this.Text = name + "*";
        }

        public ChampionshipForm(string path) : this()
        {
            XmlSerializer tx = new XmlSerializer(typeof(BindingList<Team>));
            XmlSerializer gx = new XmlSerializer(typeof(BindingList<Game>));

            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);
            BinaryReader br = new BinaryReader(fs);

            long teams_length = br.ReadInt64(), games_length = br.ReadInt64();

            if (fs.Length != (teams_length + games_length + 16))
            {
                MessageBox.Show("Обраний файл не є коректним файлом чемпіонату, або він пошкодженний", "Помилка відкриття файлу", MessageBoxButtons.OK, MessageBoxIcon.Error);

                this.OpenOk = false;
                return;
            }

            byte[] buf = new byte[teams_length];
            fs.Read(buf, 0, (int)teams_length);

            MemoryStream ts = new MemoryStream(buf);
            ts.Position = 0;
            teams = (BindingList<Team>)tx.Deserialize(ts);
            ts.Close();

            Game.Teams = teams;
            BindTeams();

            buf = new byte[games_length];
            fs.Read(buf, 0, (int)games_length);

            MemoryStream gs = new MemoryStream(buf);
            gs.Position = 0;
            games = (BindingList<Game>)gx.Deserialize(gs);
            gs.Close();

            BindGames();

            br.Close();

            name = System.IO.Path.GetFileNameWithoutExtension(path);
            this.Text = name;

            this.OpenOk = true;
            this.path = path;
        }

        private void FixColumns()
        {
            ImgColumn.DisplayIndex = 0;
            NameColumn.DisplayIndex = 1;
            RatingELOColumn.DisplayIndex = 2;
            RatingNNetworkColumn.DisplayIndex = 3;
            WinColumn.DisplayIndex = 4;
            DefeatColumn.DisplayIndex = 5;
            DrawColumn.DisplayIndex = 6;

            emptyTeamsColumn.DisplayIndex = 7;
            emptyTeamsColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        private void BindTeams()
        {
            teamsDataGridView.AutoGenerateColumns = false;

            ImgColumn.DataPropertyName = "SmallImage";
            NameColumn.DataPropertyName = "Name";
            RatingNNetworkColumn.DataPropertyName = "RatingNNetwork";
            RatingELOColumn.DataPropertyName = "RatingELO";
            WinColumn.DataPropertyName = "Win";
            DefeatColumn.DataPropertyName = "Defeat";
            DrawColumn.DataPropertyName = "Draw";

            teamsDataGridView.DataSource = teams;
        }

        private void BindGames()
        {
            gamesDataGridView.AutoGenerateColumns = false;

            ATeamColumn.DataPropertyName = "A";
            BTeamColumn.DataPropertyName = "B";
            DateColumn.DataPropertyName = "Date";
            MatchColumn.DataPropertyName = "MatchEnd";
            PredictionColumn.DataPropertyName = "Prediction";

            gamesDataGridView.DataSource = games;
        }

        private void MarkWindow() 
        {
            if (!this.Text.EndsWith("*"))
                this.Text += "*";
        }

        #region Team Actions

        private void AddTeam()
        {
            EditTeamForm etf = new EditTeamForm();

            if (etf.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                teams.Add((Team)etf.Tag);
                MarkWindow();
            }
        }

        private void EditTeam()
        {
            DataGridViewCell dgvc = teamsDataGridView.CurrentCell;
            if (dgvc != null)
            {
                if(new EditTeamForm(teams[dgvc.RowIndex]).ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
                    MarkWindow();
            }
        }

        private void DeleteTeam() 
        {
            if (MessageBox.Show("Ви впевнені, що хочете видалити команду?", "Видалення команди", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes) 
            {
                DataGridViewCell dgvc = teamsDataGridView.CurrentCell;
                if (dgvc != null)
                {
                    Team team = teams[dgvc.RowIndex];

                    var relatedGames = games.Where(x => (x.TeamA == team || x.TeamB == team)).ToList();

                    if (relatedGames.Count > 0)
                    {
                        if (MessageBox.Show("Команда має " + relatedGames.Count + " пов'язану(і) гру(и). Вони будуть також видалені. Ви впевнені?", "Видалення команди",
                                        MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                        {
                            foreach (Game g in relatedGames)
                                games.Remove(g);

                            teams.RemoveAt(dgvc.RowIndex);

                            UpdateRatings();
                        }
                    }
                    else 
                    {
                        teams.RemoveAt(dgvc.RowIndex);
                        MarkWindow();
                    }
                }
            }
        }

        #endregion

        #region Game Actions
        private void AddGame() 
        {
            EditGameForm egf = new EditGameForm();

            if (egf.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Game g = (Game)egf.Tag;
                games.Add(g);

                UpdateRatings();
            }
        }

        private void EditGame() 
        {
            DataGridViewCell dgvc = gamesDataGridView.CurrentCell;
            if (dgvc != null)
            {
                if(new EditGameForm(games[dgvc.RowIndex]).ShowDialog() != System.Windows.Forms.DialogResult.Cancel)
                    UpdateRatings();
            }
        }

        private void DeleteGame() 
        {
            if (MessageBox.Show("Ви впевнені, що хочете видалити гру?", "Видалення гри", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                DataGridViewCell dgvc = gamesDataGridView.CurrentCell;
                if (dgvc != null)
                {
                    games.RemoveAt(dgvc.RowIndex);
                    UpdateRatings();
                }
            }
        }

        #endregion

        #region Simple Eventhendlers

        private void addTeamButton_Click(object sender, EventArgs e)
        {
            AddTeam();
        }

        private void editTeamButton_Click(object sender, EventArgs e)
        {
            EditTeam();
        }

        private void delTeamButton_Click(object sender, EventArgs e)
        {
            DeleteTeam();
        }

        private void addGamebutton_Click(object sender, EventArgs e)
        {
            AddGame();
        }

        private void editGameButton_Click(object sender, EventArgs e)
        {
            EditGame();
        }

        private void delGameButton_Click(object sender, EventArgs e)
        {
            DeleteGame();
        }

        private void ChampionshipForm_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;

            командаToolStripMenuItem.Visible = true;
            граToolStripMenuItem.Visible = false;

            FixColumns();
        }

        private void ChampionshipForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            ((MainForm)this.MdiParent).MainForm_MdiChildClosed(this);
        }

        public void ShowMe(object sender, EventArgs e)
        {
            this.Activate();
        }

        private void teamsDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            EditTeam();
        }

        private void gamesDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            EditGame();
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            UpdateRatings();
        }

        private void ігриToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Exporter.ExportGames(games);
        }

        private void командиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Exporter.ExportTeams(teams);
        }

        private void змінитиКToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditTeam();
        }

        private void видалитиКToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeleteTeam();
        }

        private void закритиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        private void UpdateRatings()
        {
            for (int i = 0; i < teams.Count; i++)
            {
                teams[i].RatingELO = 0.0;
                teams[i].RatingNNetwork = 0.0;
                teams[i].Win = 0;
                teams[i].Defeat = 0;
                teams[i].Draw = 0;
            }

            List<Game> sortedList = games.OrderBy(x => x.Date).ToList();

            for (int i = 0; i < sortedList.Count; i++)
            {
                if (sortedList[i].Complete)
                {
                    CalcCompleteGame(sortedList[i]);
                }
            }

            for (int i = 0; i < teams.Count; i++)
                teams[i].RatingELO = Math.Round(teams[i].RatingELO, 2);

            for (int i = 0; i < sortedList.Count; i++)
            {
                if (!sortedList[i].Complete)
                {
                    CalcIncompleteGame(sortedList[i]);
                }
            }

            MarkWindow();
        }

        private void CalcIncompleteGame(Game g)
        {
            double E_A = 1 / (1 + Math.Pow(10, (g.TeamB.RatingELO - g.TeamA.RatingELO) / 400));
            double E_B = 1 / (1 + Math.Pow(10, (g.TeamA.RatingELO - g.TeamB.RatingELO) / 400));

            g.Prediction = Math.Round(E_A, 3).ToString() + " : " + Math.Round(E_B, 3).ToString();
        }

        private void CalcCompleteGame(Game g)
        {
            double S_A = Convert.ToDouble(g.MatchEnd.Split(':')[0]), // голи першої команди
                    S_B = Convert.ToDouble(g.MatchEnd.Split(':')[1]); // голи другої команди

            double W_A, W_B; //набрані очки. Перемога -- 1; Нічия -- 0.5; Поразка -- 0.0;

            double K = 20.0; //коєфіцієнт матчу. Однаковий для всіх, бо рейтинг в межах чемпіонату
            double G; //коефіцієнт різниці голів

            g.TeamA.RatingNNetwork += S_A - S_B;
            g.TeamB.RatingNNetwork += S_B - S_A;

            if (S_A > S_B)
            {
                g.TeamA.Win++;
                W_A = 1.0;

                g.TeamB.Defeat++;
                W_B = 0.0;
            }
            else if (S_B > S_A)
            {
                g.TeamB.Win++;
                W_B = 1.0;

                g.TeamA.Defeat++;
                W_A = 0.0;
            }
            else
            {
                g.TeamA.Draw++;
                g.TeamB.Draw++;

                W_A = W_B = 0.5;
            }

            int d = (int)Math.Abs((S_A - S_B));

            switch (d)
            {
                case 0:
                case 1:
                    G = 1.0;
                    break;
                case 2:
                    G = 1.5;
                    break;
                default:
                    G = (11.0 + d) / 8.0;
                    break;
            }

            double We_A = 1 / (1 + Math.Pow(10, (g.TeamB.RatingELO - g.TeamA.RatingELO) / 400)); //прогноз першої команди
            double We_B = 1 / (1 + Math.Pow(10, (g.TeamA.RatingELO - g.TeamB.RatingELO) / 400)); //прогноз другої команди

            g.TeamA.RatingELO = g.TeamA.RatingELO + K * G * (W_A - We_A);
            g.TeamB.RatingELO = g.TeamB.RatingELO + K * G * (W_B - We_B);
        }

        private void teamsDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //тицьнули по заголовку...
            if (e.RowIndex == -1 && e.ColumnIndex > 0 && e.ColumnIndex < teamsDataGridView.ColumnCount)
            {
                List<Team> sortedList;

                switch (e.ColumnIndex)
                {
                    case 1:
                        sortedList = teams.OrderBy(x => x.Name).ToList();
                        break;
                    case 2:
                        sortedList = teams.OrderBy(x => x.RatingELO).ToList();
                        break;
                    case 3:
                        sortedList = teams.OrderBy(x => x.RatingNNetwork).ToList();
                        break;
                    case 4:
                        sortedList = teams.OrderBy(x => x.Win).ToList();
                        break;
                    case 5:
                        sortedList = teams.OrderBy(x => x.Defeat).ToList();
                        break;
                    case 6:
                        sortedList = teams.OrderBy(x => x.Draw).ToList();
                        break;
                    default:
                        sortedList = new List<Team>();
                        break;
                }

                teams.Clear();
                
                int k = 1, i = 0;

                if (teamsSortOrder == SortOrder.Descending || teamsSortOrder == SortOrder.None)
                {
                    teamsSortOrder = SortOrder.Ascending;
                    teamsDataGridView.Columns[e.ColumnIndex].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
                }
                else
                {
                    k = -1;
                    i = sortedList.Count - 1;
                    teamsSortOrder = SortOrder.Descending;
                    teamsDataGridView.Columns[e.ColumnIndex].HeaderCell.SortGlyphDirection = SortOrder.Descending;
                }

                for (; i < sortedList.Count && i >= 0; i += k)
                {
                    teams.Add(sortedList[i]);
                }
            }
        }

        private void gamesDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //тицьнули по заголовку...
            if (e.RowIndex == -1 && e.ColumnIndex < 3)
            {
                List<Game> sortedList;

                switch (e.ColumnIndex)
                {
                    case 0:
                        sortedList = games.OrderBy(x => x.TeamA.Name).ToList();
                        break;
                    case 1:
                        sortedList = games.OrderBy(x => x.TeamB.Name).ToList();
                        break;
                    case 2:
                        sortedList = games.OrderBy(x => x.Date).ToList();
                        break;
                    default:
                        sortedList = new List<Game>();
                        break;
                }

                games.Clear();

                int k = 1, i = 0;

                if (gamesSortOrder == SortOrder.Descending || gamesSortOrder == SortOrder.None)
                {
                    gamesSortOrder = SortOrder.Ascending;
                    gamesDataGridView.Columns[e.ColumnIndex].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
                }
                else
                {
                    k = -1;
                    i = sortedList.Count - 1;
                    gamesSortOrder = SortOrder.Descending;
                    gamesDataGridView.Columns[e.ColumnIndex].HeaderCell.SortGlyphDirection = SortOrder.Descending;
                }

                for (; i < sortedList.Count && i >= 0; i += k)
                {
                    games.Add(sortedList[i]);
                }
            }
        }

        #region Save

        private void зберегтиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Save(this.Text.Replace("*", "") + ".txl");
        }

        private void зберегтиЯкToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveAs();
        }

        private void SaveAs()
        {
            saveFileDialog.FileName = this.Text.Replace("*", "") + ".txl";
            if (saveFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.Save(saveFileDialog.FileName);
            }
        }

        private void ChampionshipForm_ParentChanged(object sender, EventArgs e)
        {
            if (this.MdiParent != null)
                saveFileDialog = ((MainForm)this.MdiParent).saveFileDialog;
        }

        private void ChampionshipForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.Text.Contains('*')) 
            {
                var dialogRezult = MessageBox.Show("Вікно містить не збережені зміни. Бажаєте зберегти їх перед виходом?", "Незбережені зміни", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                if (dialogRezult == System.Windows.Forms.DialogResult.Yes) 
                {
                    this.Save();
                }
                else if (dialogRezult == System.Windows.Forms.DialogResult.Cancel) 
                {
                    e.Cancel = true;
                }
            }
        }

        private void Save()
        {
            if (path != null)
                this.Save(path);
            else
                this.SaveAs();
        }


        public void Save(string path)
        {
            XmlSerializer tx = new XmlSerializer(typeof(BindingList<Team>));
            MemoryStream ts = new MemoryStream();
            tx.Serialize(ts, teams);
            ts.Position = 0;

            XmlSerializer gx = new XmlSerializer(typeof(BindingList<Game>));
            MemoryStream gs = new MemoryStream();
            gx.Serialize(gs, games);
            gs.Position = 0;

            FileStream fs = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None);
            BinaryWriter bw = new BinaryWriter(fs);

            bw.Write(ts.Length);
            bw.Write(gs.Length);

            ts.CopyTo(fs);
            gs.CopyTo(fs);

            ts.Close();
            gs.Close();

            bw.Close();

            this.name = Path.GetFileNameWithoutExtension(path);
            this.Text = name;
            this.path = path;
        }

        #endregion

        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (tabControl.SelectedIndex) 
            {
                case 0:
                    командаToolStripMenuItem.Visible = true;
                    граToolStripMenuItem.Visible = false;
                    break;
                case 1:
                    командаToolStripMenuItem.Visible = false;
                    граToolStripMenuItem.Visible = true;
                    break;
                default:
                    командаToolStripMenuItem.Visible = false;
                    граToolStripMenuItem.Visible = false;
                    break;
            }
        }

        private void teamsContextMenuStrip_Opening(object sender, CancelEventArgs e)
        {
            Point p = teamsDataGridView.PointToClient(Cursor.Position);
            int y = p.Y, x = p.X;

            DataGridView.HitTestInfo info = teamsDataGridView.HitTest(x, y);

            if (info.Type != DataGridViewHitTestType.Cell)
                e.Cancel = true;
            else
                teamsDataGridView.CurrentCell = teamsDataGridView[info.ColumnIndex, info.RowIndex];
        }

        private void gamesContextMenuStrip_Opening(object sender, CancelEventArgs e)
        {
            Point p = gamesDataGridView.PointToClient(Cursor.Position);
            int y = p.Y, x = p.X;

            DataGridView.HitTestInfo info = gamesDataGridView.HitTest(x, y);

            if (info.Type != DataGridViewHitTestType.Cell)
                e.Cancel = true;
            else
                gamesDataGridView.CurrentCell = gamesDataGridView[info.ColumnIndex, info.RowIndex];
        }

        private void показатиІсторіюToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
