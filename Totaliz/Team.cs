﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Drawing;

namespace Totaliz
{
    public class Team : INotifyPropertyChanged
    {
        private string name, imgId;
        private double ratingELO, ratingNNetwork;
        private int win, defeat, draw;

        [XmlIgnore]
        public Image SmallImage { get; private set; }
        [XmlIgnore]
        public Image BigImage { get; private set; }

        [Browsable(false)]
        public string ImageId
        {
            get { return imgId; }
            set 
            {
                foreach(MyImage i in MainForm.images)
                    if (i.Id == value)
                    {
                        imgId = value;
                        this.BigImage = i.BigImage;
                        this.SmallImage = i.SmallImage;

                        onNotifyPropertyChanged("ImageId");

                        return;
                    }

                System.Diagnostics.Debug.WriteLine("Can't find image with ID: " + value);
            }
        }

        public string Name
        {
            get { return name; }
            set { name = value; onNotifyPropertyChanged("Name"); }
        }

        public double RatingNNetwork
        {
            get { return ratingNNetwork; }
            set { ratingNNetwork = value; onNotifyPropertyChanged("RatingNNetwork"); }
        }

        public double RatingELO
        {
            get { return ratingELO; }
            set { ratingELO = value; onNotifyPropertyChanged("RatingELO"); }
        }

        public int Win 
        {
            get { return win; }
            set { win = value; onNotifyPropertyChanged("Win"); }
        }

        public int Defeat
        {
            get { return defeat; }
            set { defeat = value; onNotifyPropertyChanged("Defeat"); }
        }

        public int Draw
        {
            get { return draw; }
            set { draw = value; onNotifyPropertyChanged("Draw"); }
        }

        public Team() 
        {
            
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void onNotifyPropertyChanged(String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
