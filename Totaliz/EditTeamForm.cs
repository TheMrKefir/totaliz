﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Totaliz
{
    public partial class EditTeamForm : Form
    {
        private Team team;

        private void Init()
        {
            InitializeComponent();

            logoComboBox.DisplayMember = "Name";
            logoComboBox.DataSource = MainForm.images;
        }

        public EditTeamForm()
        {
            Init();

            team = null;

            this.Text = "Додавання нової команди";
        }

        public EditTeamForm(Team t)
        {
            Init();

            team = t;

            this.Text = "Редагування команди " + t.Name;

            nameTextBox.Text = t.Name;
            for (int i = 0; i < MainForm.images.Count; i++)
			{
                if(MainForm.images[i].Id == t.ImageId)
                {
                    logoComboBox.SelectedIndex = i;
                }
			}
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void logoComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            pictureBox.Image = ((MyImage)logoComboBox.SelectedItem).BigImage;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            if (team == null)
            {
                Team t = new Team();
                t.Name = nameTextBox.Text;
                t.ImageId = ((MyImage)logoComboBox.SelectedItem).Id;
                this.Tag = t;

                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            else if (MessageBox.Show("Ви впевнені, що хочете зберегти внесені в інформацію про команду зміни?", "Редагування інформації про команду",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                team.Name = nameTextBox.Text;
                team.ImageId = ((MyImage)logoComboBox.SelectedItem).Id;

                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            else
            {
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            }
        }
    }
}
