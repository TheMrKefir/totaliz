﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Totaliz
{
    public partial class MainForm : Form
    {
        int openedWindows;
        ToolStripSeparator separ;
        public static List<MyImage> images;

        static MainForm() 
        {
            if (File.Exists(".//img//image_list.xml"))
            {
                XmlSerializer x = new XmlSerializer(typeof(List<MyImage>));
                object o = null;

                using (StreamReader sr = new StreamReader(".//img//image_list.xml"))
                {
                    try
                    {
                        o = x.Deserialize(sr);
                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.Debug.WriteLine("Error! " + e.ToString() + " \n" + e.Message);
                    }
                }

                images = (List<MyImage>)o;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("File image_list.xml not found!");
                images = new List<MyImage>();
            }
        }

        public MainForm()
        {
            openedWindows = 0;
            separ = new ToolStripSeparator();
            separ.MergeIndex = 12;

            InitializeComponent();

            this.MdiChildActivate += MainForm_MdiChildActivate;
        }

        void MainForm_MdiChildActivate(object sender, EventArgs e)
        {
            
        }

        public void MainForm_MdiChildClosed(object sender)
        {
            файлToolStripMenuItem.DropDownItems.Remove(((ChampionshipForm)sender).MenuItem);

            if (this.MdiChildren.Length == 1)
            {
                файлToolStripMenuItem.DropDownItems.Remove(separ);
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new AboutBox().ShowDialog();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChampionshipForm cf = new ChampionshipForm(++openedWindows);
            cf.MdiParent = this;
            cf.Show();

            AddItem(cf);
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK) 
            {
                if(Path.GetExtension(openFileDialog.FileName) != ".txl" &&
                    MessageBox.Show("Ви намагаєтесь відкрити файл з невідомим розширенням. Ви впевнені, що хочете продовжити?",
                                    "На відоме розширення файлу", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != System.Windows.Forms.DialogResult.Yes)
                {
                    return;
                }

                ChampionshipForm cf = new ChampionshipForm(openFileDialog.FileName);

                if (cf.OpenOk)
                {
                    cf.MdiParent = this;
                    cf.Show();

                    AddItem(cf);
                }
            }
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            файлToolStripMenuItem.DropDownItems.Remove(((ChampionshipForm)this.ActiveMdiChild).MenuItem);
            this.ActiveMdiChild.Close();

            if (this.MdiChildren.Length == 0)
            {
                файлToolStripMenuItem.DropDownItems.Remove(separ);
            }
        }

        private void AddItem(ChampionshipForm f) 
        {
            if (this.MdiChildren.Length == 1)
                файлToolStripMenuItem.DropDownItems.Add(separ);

            ToolStripMenuItem tsmi = new ToolStripMenuItem(f.Text);
            tsmi.MergeIndex = 12 + this.MdiChildren.Length;
            tsmi.Click += f.ShowMe;
            tsmi.Size = new Size(170, 22);
            f.MenuItem = tsmi;

            файлToolStripMenuItem.DropDownItems.Add(tsmi);
        }
    }
}
