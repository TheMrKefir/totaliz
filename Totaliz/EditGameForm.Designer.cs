﻿namespace Totaliz
{
    partial class EditGameForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.teamALabel = new System.Windows.Forms.Label();
            this.teamAComboBox = new System.Windows.Forms.ComboBox();
            this.teamBComboBox = new System.Windows.Forms.ComboBox();
            this.teamBLabel = new System.Windows.Forms.Label();
            this.completeCheckBox = new System.Windows.Forms.CheckBox();
            this.matchLabel = new System.Windows.Forms.Label();
            this.teamANumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.teamBNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label = new System.Windows.Forms.Label();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.dateLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.teamANumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamBNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // teamALabel
            // 
            this.teamALabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.teamALabel.AutoSize = true;
            this.teamALabel.Location = new System.Drawing.Point(12, 15);
            this.teamALabel.Name = "teamALabel";
            this.teamALabel.Size = new System.Drawing.Size(68, 13);
            this.teamALabel.TabIndex = 0;
            this.teamALabel.Text = "Команда А: ";
            // 
            // teamAComboBox
            // 
            this.teamAComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.teamAComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.teamAComboBox.FormattingEnabled = true;
            this.teamAComboBox.Location = new System.Drawing.Point(86, 12);
            this.teamAComboBox.Name = "teamAComboBox";
            this.teamAComboBox.Size = new System.Drawing.Size(186, 21);
            this.teamAComboBox.TabIndex = 1;
            this.teamAComboBox.SelectedIndexChanged += new System.EventHandler(this.teamAComboBox_SelectedIndexChanged);
            // 
            // teamBComboBox
            // 
            this.teamBComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.teamBComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.teamBComboBox.FormattingEnabled = true;
            this.teamBComboBox.Location = new System.Drawing.Point(86, 39);
            this.teamBComboBox.Name = "teamBComboBox";
            this.teamBComboBox.Size = new System.Drawing.Size(186, 21);
            this.teamBComboBox.TabIndex = 3;
            this.teamBComboBox.SelectedIndexChanged += new System.EventHandler(this.teamBComboBox_SelectedIndexChanged);
            // 
            // teamBLabel
            // 
            this.teamBLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.teamBLabel.AutoSize = true;
            this.teamBLabel.Location = new System.Drawing.Point(12, 42);
            this.teamBLabel.Name = "teamBLabel";
            this.teamBLabel.Size = new System.Drawing.Size(68, 13);
            this.teamBLabel.TabIndex = 2;
            this.teamBLabel.Text = "Команда B: ";
            // 
            // completeCheckBox
            // 
            this.completeCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.completeCheckBox.AutoSize = true;
            this.completeCheckBox.Location = new System.Drawing.Point(12, 93);
            this.completeCheckBox.Name = "completeCheckBox";
            this.completeCheckBox.Size = new System.Drawing.Size(70, 17);
            this.completeCheckBox.TabIndex = 4;
            this.completeCheckBox.Text = "Зіграний";
            this.completeCheckBox.UseVisualStyleBackColor = true;
            this.completeCheckBox.CheckedChanged += new System.EventHandler(this.completeCheckBox_CheckedChanged);
            // 
            // matchLabel
            // 
            this.matchLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.matchLabel.AutoSize = true;
            this.matchLabel.Enabled = false;
            this.matchLabel.Location = new System.Drawing.Point(91, 94);
            this.matchLabel.Name = "matchLabel";
            this.matchLabel.Size = new System.Drawing.Size(51, 13);
            this.matchLabel.TabIndex = 5;
            this.matchLabel.Text = "Рахунок:";
            // 
            // teamANumericUpDown
            // 
            this.teamANumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.teamANumericUpDown.Enabled = false;
            this.teamANumericUpDown.Location = new System.Drawing.Point(148, 92);
            this.teamANumericUpDown.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.teamANumericUpDown.Name = "teamANumericUpDown";
            this.teamANumericUpDown.Size = new System.Drawing.Size(55, 20);
            this.teamANumericUpDown.TabIndex = 6;
            // 
            // teamBNumericUpDown
            // 
            this.teamBNumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.teamBNumericUpDown.Enabled = false;
            this.teamBNumericUpDown.Location = new System.Drawing.Point(217, 92);
            this.teamBNumericUpDown.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.teamBNumericUpDown.Name = "teamBNumericUpDown";
            this.teamBNumericUpDown.Size = new System.Drawing.Size(55, 20);
            this.teamBNumericUpDown.TabIndex = 7;
            // 
            // label
            // 
            this.label.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label.AutoSize = true;
            this.label.Enabled = false;
            this.label.Font = new System.Drawing.Font("Moire", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label.Location = new System.Drawing.Point(204, 91);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(13, 19);
            this.label.TabIndex = 8;
            this.label.Text = ":";
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.Location = new System.Drawing.Point(116, 119);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 9;
            this.okButton.Text = "Ok";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(197, 119);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 10;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // dateTimePicker
            // 
            this.dateTimePicker.Location = new System.Drawing.Point(57, 66);
            this.dateTimePicker.Name = "dateTimePicker";
            this.dateTimePicker.Size = new System.Drawing.Size(215, 20);
            this.dateTimePicker.TabIndex = 11;
            // 
            // dateLabel
            // 
            this.dateLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateLabel.AutoSize = true;
            this.dateLabel.Location = new System.Drawing.Point(12, 69);
            this.dateLabel.Name = "dateLabel";
            this.dateLabel.Size = new System.Drawing.Size(39, 13);
            this.dateLabel.TabIndex = 12;
            this.dateLabel.Text = "Дата: ";
            // 
            // EditGameForm
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(284, 154);
            this.Controls.Add(this.dateLabel);
            this.Controls.Add(this.dateTimePicker);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.label);
            this.Controls.Add(this.teamBNumericUpDown);
            this.Controls.Add(this.teamANumericUpDown);
            this.Controls.Add(this.matchLabel);
            this.Controls.Add(this.completeCheckBox);
            this.Controls.Add(this.teamBComboBox);
            this.Controls.Add(this.teamBLabel);
            this.Controls.Add(this.teamAComboBox);
            this.Controls.Add(this.teamALabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "EditGameForm";
            this.Text = "EditGameForm";
            ((System.ComponentModel.ISupportInitialize)(this.teamANumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamBNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label teamALabel;
        private System.Windows.Forms.ComboBox teamAComboBox;
        private System.Windows.Forms.ComboBox teamBComboBox;
        private System.Windows.Forms.Label teamBLabel;
        private System.Windows.Forms.CheckBox completeCheckBox;
        private System.Windows.Forms.Label matchLabel;
        private System.Windows.Forms.NumericUpDown teamANumericUpDown;
        private System.Windows.Forms.NumericUpDown teamBNumericUpDown;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.DateTimePicker dateTimePicker;
        private System.Windows.Forms.Label dateLabel;
    }
}