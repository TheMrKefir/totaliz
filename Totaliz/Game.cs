﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Totaliz
{
    public class Game : INotifyPropertyChanged
    {
        [XmlIgnore]
        public static BindingList<Team> Teams { get; set; }
        private string a, b, matchEnd, prediction;
        private bool complete;
        private DateTime date;

        private PropertyChangedEventHandler TeamAChanged, TeamBChanged;

        public string A
        {
            get { return a; }
            set 
            {
                if (TeamA != null)
                    TeamA.PropertyChanged -= TeamAChanged;

                a = value;

                if(Teams != null)
                    foreach (Team t in Teams)
                        if (t.Name == value) 
                        {
                            TeamA = t;

                            TeamA.PropertyChanged += TeamAChanged;

                            break;
                        }
                
                onNotifyPropertyChanged("A");
            }
        }

        void TeamA_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            a = TeamA.Name;
            onNotifyPropertyChanged("A");
        }

        public string B
        {
            get { return b; }
            set 
            {
                if (TeamB != null)
                    TeamB.PropertyChanged -= TeamBChanged;

                b = value;

                if (Teams != null)
                    foreach (Team t in Teams)
                        if (t.Name == value)
                        {
                            TeamB = t;

                            TeamB.PropertyChanged += TeamBChanged;
                            
                            break;
                        }

                onNotifyPropertyChanged("B");
            }
        }

        void TeamB_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            b = TeamB.Name;
            onNotifyPropertyChanged("B");
        }

        [XmlIgnore]    
        public Team TeamA { get; private set; }
        [XmlIgnore]
        public Team TeamB { get; private set; }

        public string MatchEnd 
        { 
            get { return matchEnd;}
            set { matchEnd = value; onNotifyPropertyChanged("MatchEnd"); }
        }

        public string Prediction
        {
            get { return prediction; }
            set { prediction = value; onNotifyPropertyChanged("Prediction"); }
        }

        public bool Complete
        {
            get { return complete; }
            set { complete = value; onNotifyPropertyChanged("Complete"); }
        }

        public DateTime Date
        {
            get { return date; }
            set { date = value; onNotifyPropertyChanged("Date"); }
        }

        public Game() 
        {
            TeamAChanged = new PropertyChangedEventHandler(TeamA_PropertyChanged);
            TeamBChanged = new PropertyChangedEventHandler(TeamB_PropertyChanged);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void onNotifyPropertyChanged(String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
