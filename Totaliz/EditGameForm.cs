﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Totaliz
{
    public partial class EditGameForm : Form
    {
        private Game game;

        public EditGameForm()
        {
            Init();

            game = null;

            this.Text = "Додавання нової гри";

            dateTimePicker.Value = DateTime.Today;
        }

        public EditGameForm(Game g) 
        {
            Init();

            game = g;

            for (int i = 0; i < Game.Teams.Count; i++)
            {
                if (Game.Teams[i].Name == g.A)
                {
                    teamAComboBox.SelectedIndex = i;
                }
            }

            for (int i = 0; i < Game.Teams.Count; i++)
            {
                if (Game.Teams[i].Name == g.B)
                {
                    teamBComboBox.SelectedIndex = i;
                }
            }

            completeCheckBox.Checked = g.Complete;

            if (g.Complete)
            {
                teamANumericUpDown.Value = Convert.ToDecimal(g.MatchEnd.Split(':')[0]);
                teamBNumericUpDown.Value = Convert.ToDecimal(g.MatchEnd.Split(':')[1]);
            }

            if (g.Date < dateTimePicker.MinDate || g.Date > dateTimePicker.MaxDate)
            {
                dateTimePicker.Value = DateTime.Today;
            }
            else
            {
                dateTimePicker.Value = g.Date;
            }

            this.Text = "Редагування гри " + game.A + " vs " + game.B;
        }

        private void Init()
        {
            InitializeComponent();

            
            teamAComboBox.DisplayMember = "Name";
            teamAComboBox.DataSource = new List<Team>(Game.Teams);
            teamAComboBox.SelectedIndex = -1;
            
            teamBComboBox.DisplayMember = "Name";
            teamBComboBox.DataSource = new List<Team>(Game.Teams);
            teamBComboBox.SelectedIndex = -1;
        }

        private void completeCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            teamANumericUpDown.Enabled = completeCheckBox.Checked;
            teamBNumericUpDown.Enabled = completeCheckBox.Checked;
            matchLabel.Enabled = completeCheckBox.Checked;
            label.Enabled = completeCheckBox.Checked;
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            if (game == null)
            {
                Game g = new Game();
                g.A = ((Team)teamAComboBox.SelectedItem).Name;
                g.B = ((Team)teamBComboBox.SelectedItem).Name;

                g.Date = dateTimePicker.Value;

                if (completeCheckBox.Checked)
                {
                    g.Complete = true;
                    g.MatchEnd = teamANumericUpDown.Value.ToString() + ":" +
                        teamBNumericUpDown.Value.ToString();
                }

                this.Tag = g;

                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            else if (MessageBox.Show("Ви впевнені, що хочете зберегти внесені в гру зміни?", "Редагування гри",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                game.A = ((Team)teamAComboBox.SelectedItem).Name;
                game.B = ((Team)teamBComboBox.SelectedItem).Name;

                game.Date = dateTimePicker.Value;

                if (completeCheckBox.Checked)
                {
                    game.Complete = true;
                    game.MatchEnd = teamANumericUpDown.Value.ToString() + ":" +
                        teamBNumericUpDown.Value.ToString();
                    game.Prediction = "";
                }
                else
                {
                    game.Complete = false;
                    game.MatchEnd = "";
                }

                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            else
            {
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void teamAComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (teamAComboBox.SelectedIndex != -1 && teamBComboBox.SelectedIndex == teamAComboBox.SelectedIndex)
                teamBComboBox.SelectedIndex = -1;
        }

        private void teamBComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (teamBComboBox.SelectedIndex != -1 && teamAComboBox.SelectedIndex == teamBComboBox.SelectedIndex)
                teamAComboBox.SelectedIndex = -1;
        }
    }
}
